# [Setup, Getting Started, And More Here](../../wiki/Home)

# About the Author(s), Contributors
|      |      |
|---------|---------|
||[Chris Cilino](https://bit.ly/ChrisCilino_LinkedIn): I have a passion for technology, teamwork, and mentorship. In my 20+ year career, I've employed all three in the context of using LabVIEW in creating applications and engaging with the LabVIEW community. I'm proud to serve the LabVIEW community as one of its [Champions](http://bit.ly/ChrisCilino_ChampionProfile). |
||[Siva Shankar](https://www.linkedin.com/in/siva-shankar-93855bba)|
||[PADMANABAN JAYARAMAN](https://www.linkedin.com/in/padmanaban-jayaraman-b33408124/)|



Please feel free to check out 

* [This Repository's Wiki](../../wiki/Home)
* [My Online Presentations](http://bit.ly/ChrisCilino_Presentations)
* [My Free and Open Source Code](http://bit.ly/ChrisCilino_CSuite)
* [My LinkedIn Profile](http://bit.ly/ChrisCilino_LinkedIn)


# License
All software in this repository is licensed under the MIT license found in [..\Build\License\License.rtf](../master/Build/License/License.rtf). 